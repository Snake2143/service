# Модуль: Zabbix

## Status

### Принятый

## Context

### Zabbix — свободная система мониторинга статусов разнообразных сервисов компьютерной сети, серверов и сетевого оборудования, написанная Алексеем Владышевым. Для хранения данных используется MySQL, PostgreSQL, SQLite или Oracle Database, веб-интерфейс написан на PHP. Поддерживает несколько видов мониторинга:
### Simple checks — может проверять доступность и реакцию стандартных сервисов, таких как SMTP или HTTP, без установки какого-либо программного обеспечения на наблюдаемом хосте. Zabbix agent — может быть установлен на UNIX-подобных или Windows-хостах для получения данных о нагрузке процессора, использования сети, дисковом пространстве и так далее.External check — выполнение внешних программ, также поддерживается мониторинг через SNMP.Основные возможности:
### Распределённый мониторинг — до нескольких тысяч узлов. Конфигурация младших узлов полностью контролируется старшими узлами, находящимися на более высоком ### уровне иерархии
### Сценарии на основе мониторинга
### Автоматическое обнаружение
### Централизованный мониторинг журналов
### Веб-интерфейс для администрирования и настройки
### Отчётность и тенденции
### SLA-мониторинг
### Поддержка высокопроизводительных агентов (zabbix-agent) практически для всех платформ
### Комплексная реакция на события
### Поддержка SNMP v1, 2, 3
### Поддержка SNMP-ловушек
### Поддержка IPMI
### Поддержка мониторинга JMX-приложений
### Поддержка выполнения запросов в различные базы данных без необходимости использования сценарной обвязки
### Расширение за счёт выполнения внешних скриптов
### Гибкая система шаблонов и групп
### Возможность создавать карты сетей
### Интеграция с внешними системами с помощью плагинов. Например, Zabbix можно интегрировать в Grafana для визуализации данных, построения графиков и дашбордов.
### Отдельный блок возможностей связан с автоматическим обнаружением: устройств по диапазону IP-адресов, доступных на них сервисах, также реализована SNMP-проверка. Обеспечивается автоматический мониторинг обнаруженных устройств, автоматическое удаление отсутствующих узлов, распределение по группам и шаблонам в зависимости от возвращаемого результата. Низкоуровневое обнаружение может быть использовано для обнаружения и для начала мониторинга файловых систем, сетевых интерфейсов. Начиная с Zabbix 2.0, поддерживаются три встроенных механизма низкоуровневого обнаружения:
### обнаружение файловых систем;
### обнаружение сетевых интерфейсов;
### обнаружение нескольких SNMP OID.
### Поддерживаемые платформы (сервер и агент): AIX, FreeBSD, HP-UX, Linux, macOS, OpenBSD, SCO OpenServer, Solaris, Tru64/OSF; кроме того, реализованы агенты для Novell Netware и операционных систем семейства Windows.
## Decision
### Установка и развертывание происходит из контейнера с помощью Docker
## Consequences
### На сервере развернута система мониторинга Zabbix.